import { createMuiTheme } from "@material-ui/core/styles";

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: "rgb(6, 123, 147)",
    },
    secondary: {
      main: "#e10a0a",
    },
  },
  typography: {
    fontFamily: "Comfortaa",
  },

  overrides: {
    // Style sheet name ⚛️
    MuiButton: {
      // Name of the rule
    },

    MuiContainer: {
      root: {
        paddingLeft: 0,
        paddingRight: 0,
      },
    },
    MuiSvgIcon: {
      root: {
        fontSize: 30,
      },
    },
    MuiInput: {
      underline: {
        "&:after": {
          borderBottom: "2px solid rgb(6, 123, 147)",
        },
      },
    },

    MuiOutlinedInput: {
      root: {
        "&$focused $notchedOutline": {
          borderColor: "rgb(6, 123, 147)",
        },
        "&$focused $notchedOutlineText": {
          color: "rgb(6, 123, 147)",
        },
      },
    },
  },
});
