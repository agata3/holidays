import { MuiThemeProvider } from "@material-ui/core/styles";
import { theme } from "../MuiTheme";
import { FullDialog } from "./FullDialog";
import Container from "@material-ui/core/Container";
import { Route } from "react-router-dom";
import { pages } from "../config";

export const App = () => {
  return (
    <MuiThemeProvider theme={theme}>
      <Container maxWidth="lg">
        <FullDialog />
        {pages.map((page) => (
          <Route exact key={page.title} path={page.url}>
            {page.component}
          </Route>
        ))}
      </Container>
    </MuiThemeProvider>
  );
};

export default App;
