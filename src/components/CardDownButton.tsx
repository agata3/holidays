import { makeStyles } from "@material-ui/core/styles";
import { Card, Box } from "@material-ui/core";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { Deal } from "../utils/dealsData";
const useStyles = makeStyles({
  root: {
    marginTop: 10,
    marginBottom: 10,
    paddingBottom: 10,
  },
});

interface CardDownButtonProps {
  item: Deal;
}

export const CardDownButton: React.FC<CardDownButtonProps> = ({ item }) => {
  const classes = useStyles();
  return (
    <Box mt={2} mb={2} display="flex">
      <Card className={classes.root}>
        <CardActionArea>
          <CardMedia
            component="img"
            alt="Contemplative Reptile"
            height="210"
            image={item.backgroundImage}
            title="Contemplative Reptile"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {item.title}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {item.text}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button
            size="medium"
            color="primary"
            fullWidth
            variant="contained"
            style={{
              backgroundColor: "#e10a0a",
              marginLeft: 20,
              marginRight: 20,
              fontSize: 13,
            }}
          >
            View {item.title}
          </Button>
        </CardActions>
      </Card>
    </Box>
  );
};
