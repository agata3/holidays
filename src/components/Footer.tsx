import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

export const Footer = () => {
  return (
    <div>
      <Box display="flex" p={2} style={{ backgroundColor: "#f5f5f5" }}>
        <Typography component="div">
          <Box fontSize={12} m={1}>
            <strong>TRAVEL AWARE – STAYING SAFE AND HEALTHY ABROAD</strong>
          </Box>
          <Box textAlign="justify" m={1} fontSize={10}>
            Your holiday is protected – many of the flights and flight-inclusive
            .
          </Box>
          <Box textAlign="left" m={1} fontSize={10}>
            Protection does not apply to all{" "}
            <i style={{ color: "#067b93" }}>holiday and travel services</i>{" "}
            listed on this website.
          </Box>
          <Box textAlign="center" m={1} fontSize={10}>
            Please see our booking conditions for information or for more.
          </Box>
        </Typography>
      </Box>
    </div>
  );
};
