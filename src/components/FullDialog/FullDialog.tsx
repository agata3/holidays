import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import { Box } from "@material-ui/core";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import { TransitionProps } from "@material-ui/core/transitions";
import { ListMenu } from "./ListMenu";
import { IconMenu } from "./IconMenu";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      position: "relative",
      backgroundColor: theme.palette.background.paper,
    },
    title: {
      marginLeft: theme.spacing(),
    },
    root: {
      backgroundColor: theme.palette.background.paper,
    },
  })
);

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} timeout={2500} {...props} />;
});

export const FullDialog = () => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Box className={classes.root}>
      <IconMenu handleClickOpen={handleClickOpen} />
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <div style={{ width: "100%" }} className={classes.appBar}>
          <Box
            display="flex"
            p={1}
            alignItems="center"
            justifyContent="flex-end"
          >
            <Toolbar>
              <Box p={1} component="div">
                <Typography
                  className={classes.title}
                  color="secondary"
                  component="div"
                >
                  <Box fontWeight="fontWeightBold" fontSize="h6.fontSize">
                    {" "}
                    close{" "}
                  </Box>
                </Typography>
              </Box>

              <Box>
                <IconButton
                  color="secondary"
                  onClick={handleClose}
                  aria-label="close"
                >
                  <CloseIcon fontSize="large" />
                </IconButton>
              </Box>
            </Toolbar>
          </Box>
        </div>

        <ListMenu handleClose={handleClose} />
      </Dialog>
    </Box>
  );
};
