import MenuIcon from "@material-ui/icons/Menu";
import { Box, Button } from "@material-ui/core";
import { createStyles, makeStyles } from "@material-ui/core/styles";
const useStyles = makeStyles(() =>
  createStyles({
    root: {
      marginBottom: 20,
      marginTop: -5,
    },
    img: {
      width: 130,
      height: "auto",
    },
    menuicon: {
      width: 45,
      height: "auto",
    },
  })
);

export interface ListMenuProps {
  handleClickOpen?: any;
}
export const IconMenu: React.FC<ListMenuProps> = ({ handleClickOpen }) => {
  const classes = useStyles();
  return (
    <div style={{ width: "100%" }} className={classes.root}>
      <Box display="flex" justifyContent="flex-start" p={2}>
        <Box flexGrow={1} alignSelf="flex-end">
          <img
            className={classes.img}
            src="//www.virginholidays.co.uk/dam/jcr:a25a2285-733f-4797-8d81-b1b1115efada"
            alt="Virgin holidays logo"
          />
        </Box>
        <Box>
          <Button onClick={handleClickOpen}>
            <MenuIcon color="secondary" className={classes.menuicon}></MenuIcon>
          </Button>
        </Box>
      </Box>
    </div>
  );
};
