import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import { pages } from "../../config";
import { Link, Box } from "@material-ui/core";
import { Link as RouterLink } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      maxWidth: 360,
      backgroundColor: theme.palette.background.paper,
    },
  })
);
const text = {
  fontSize: "1.1em",
  fontWeight: 800,
};

export interface ListMenuProps {
  handleClose?: any;
}

export const ListMenu: React.FC<ListMenuProps> = ({ handleClose }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <List component="nav" aria-label="main mailbox folders">
        {pages
          //   .filter((page) => page.title !== "Home")
          .map((page) => {
            return (
              <Box key={page.title}>
                <Divider />

                <Link
                  key={page.title}
                  component={RouterLink}
                  onClick={handleClose}
                  to={page.url}
                  // to={`/${
                  //   page.title === "Home" ? "" : page.title.toLowerCase()
                  // }`}

                  color="inherit"
                >
                  <div style={{ width: "100%" }}>
                    <Box p={1} display="flex" justifyContent="flex-start">
                      <Box>
                        <ListItem
                          button
                          // onClick={() => {
                          //   handleClose();
                          //   push(page.url);
                          // }}
                        >
                          <ListItemText
                            primaryTypographyProps={{ style: text }}
                            primary={page.title}
                          />
                        </ListItem>
                      </Box>
                    </Box>
                  </div>
                </Link>
              </Box>
            );
          })}
        <Divider />
      </List>
    </div>
  );
};
