import { Box, Grid } from "@material-ui/core";

export interface BackgroundImage {
  backgroundImage: string;
  height: number;
  backgroundPositionX?: number;
  backgroundPositionY?: number;
  width?: number | string;
}

export const MainPicture: React.FC<BackgroundImage> = ({
  backgroundImage,
  height,
  backgroundPositionX,
  backgroundPositionY,
  width,
}) => {
  return (
    <Box display="flex">
      <Grid container justify="center">
        <Grid
          item
          xs={12}
          style={{
            backgroundImage: `url(${backgroundImage})`,
            backgroundSize: "cover",
            height,
            borderRadius: 2,
            backgroundPositionX,
            backgroundPositionY,
            width,
          }}
        ></Grid>
      </Grid>
    </Box>
  );
};
