import { Box, Typography } from "@material-ui/core";

export interface MainTextProps {
  mainText?: string;
  secondaryText: string;
}

export const MainText: React.FC<MainTextProps> = ({
  mainText,
  secondaryText,
}) => {
  return (
    <Box display="flex" p={2}>
      <Typography component="div">
        <Box
          fontSize={28}
          m={2}
          textAlign="center"
          style={{ color: "black" }}
          fontWeight={800}
        >
          {mainText}
        </Box>
        <Box textAlign="justify" m={2} fontSize={14}>
          {secondaryText}
        </Box>
      </Typography>
    </Box>
  );
};
