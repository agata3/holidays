import { Box, Grid, Typography } from "@material-ui/core";

export interface MainTextPictureProps {
  backgroundImage: string;
  height: number;
  text?: string;
  backgroundPositionY?: number;
  backgroundPositionX?: number;
}

export const MainTextPicture: React.FC<MainTextPictureProps> = ({
  backgroundImage,
  height,
  text,
  backgroundPositionY,
  backgroundPositionX,
}) => {
  return (
    <Box display="flex" position="relative">
      <Grid container justify="center">
        <Grid
          item
          xs={12}
          style={{
            backgroundImage: `url(${backgroundImage})`,
            backgroundSize: "cover",
            height,
            borderRadius: 2,
            backgroundPositionY,
            backgroundPositionX,
          }}
        ></Grid>
      </Grid>
      <Box position="absolute" alignSelf="center">
        <Typography component="div">
          <Box
            p={2}
            fontSize={40}
            textAlign="center"
            fontWeight={900}
            style={{
              color: "#FFF",
              textShadow: "2px 2px #424242",
            }}
          >
            {text}
          </Box>
        </Typography>
      </Box>
    </Box>
  );
};
