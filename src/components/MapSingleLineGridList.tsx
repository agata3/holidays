import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import { IconButton, GridList } from "@material-ui/core";
import { Deal } from "../utils/dealsData";

const useStyles = makeStyles(() =>
  createStyles({
    title: {
      color: "#e10a0a",
      fontWeight: "bolder",
      fontSize: 20,
    },
    titleBar: {
      padding: 5,
      background: "linear-gradient(to bottom, #ffff, #eeeeee)",
    },
    gridList: {
      borderRadius: 5,
      flexWrap: "nowrap",
      // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
      transform: "translateZ(0)",
    },
  })
);
export interface List {
  list: Deal[];
}

export const MapSingleLineGridList: React.FC<List> = ({ list }) => {
  const classes = useStyles();
  return (
    <>
      <GridList
        className={classes.gridList}
        cols={1.2}
        cellHeight={280}
        spacing={15}
      >
        {list.map((item) => (
          <GridListTile key={item.backgroundImage}>
            <img src={item.backgroundImage} alt={item.title} />
            <GridListTileBar
              title={item.title}
              classes={{
                root: classes.titleBar,
                title: classes.title,
              }}
              actionIcon={
                <IconButton style={{ color: "#e10a0a" }}>
                  {<ArrowForwardIosIcon fontSize="large" />}
                </IconButton>
              }
            />
          </GridListTile>
        ))}
      </GridList>
    </>
  );
};
