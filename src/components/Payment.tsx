import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import payments from "./../assets/payments.jpg";

export default function Payment() {
  return (
    <div style={{ width: "100%" }}>
      <Box display="flex" p={1} bgcolor="background.paper">
        <Typography component="div">
          <Box mt={1} fontWeight="bold" fontSize={13}>
            Payment methods
          </Box>
          <Box
            style={{
              backgroundImage: `url(${payments})`,
              backgroundSize: "cover",
              height: 180,
              marginTop: -40,
              marginLeft: -85,
            }}
          ></Box>

          <Box fontSize={11} mt={1}>
            <strong>
              Bookings made by credit or debit card no longer credit incur a
              fee.
            </strong>{" "}
            <Box mt={1}>
              Payment by donec sed odio operae, eu vulputate felis rhoncus.
            </Box>
          </Box>
        </Typography>
      </Box>
    </div>
  );
}
