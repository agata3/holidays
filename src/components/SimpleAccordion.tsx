import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { Info } from "../utils/infoData";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightBold,
    },
    textSecondary: {
      fontSize: theme.typography.pxToRem(13),
    },
  })
);
export interface AccordionProps {
  item: Info;
}

export const SimpleAccordion: React.FC<AccordionProps> = ({ item }) => {
  const classes = useStyles();

  return (
    <div className={classes.root} key={item.key}>
      <Accordion>
        <AccordionSummary
          expandIcon={
            <ExpandMoreIcon style={{ color: item.color, fontSize: 40 }} />
          }
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading}>{item.mainText}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className={classes.textSecondary}>
            {item.secondaryText}
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};
