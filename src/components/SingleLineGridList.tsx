import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import { Box, Typography, IconButton } from "@material-ui/core";
import RemoveIcon from "@material-ui/icons/Remove";
import { Deal } from "../utils/dealsData";
import { MapSingleLineGridList } from "./MapSingleLineGridList";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginTop: 30,
      marginBottom: 30,
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      backgroundColor: theme.palette.background.paper,
    },
  })
);

export interface SingleLineGridListProps {
  list: Deal[];
}

export const SingleLineGridList: React.FC<SingleLineGridListProps> = ({
  list,
}) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div style={{ width: "100%" }}>
        <Box
          m={2}
          mb={3}
          display="flex"
          flexDirection="column"
          justifyContent="center"
        >
          <Box alignSelf="center" position="relative" m={1} mb={2}>
            <Typography component="div">
              <Box fontWeight={900} fontSize={30}>
                Top offers
              </Box>
            </Typography>
          </Box>

          <Box alignSelf="center" position="absolute">
            <IconButton style={{ color: "#e10a0a", bottom: -30 }}>
              {<RemoveIcon style={{ fontSize: 65 }} />}
            </IconButton>
          </Box>
        </Box>
      </div>

      <MapSingleLineGridList list={list} />
    </div>
  );
};
