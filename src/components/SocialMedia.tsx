import { Box, Icon } from "@material-ui/core";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";
import InstagramIcon from "@material-ui/icons/Instagram";
import YouTubeIcon from "@material-ui/icons/YouTube";

const socialList = [
  { id: 1, icon: <FacebookIcon />, color: "#4267b2" },
  { id: 2, icon: <TwitterIcon />, color: "#1da1f2" },
  { id: 3, icon: <InstagramIcon />, color: "#ab47bc" },
  { id: 4, icon: <YouTubeIcon />, color: "rgb(229, 45, 39)" },
];

export const SocialMedia = () => {
  return (
    <Box
      display="flex"
      p={1}
      bgcolor="background.paper"
      mt={2}
      mb={2}
      justifyContent="space-between"
    >
      {socialList.map((item) => (
        <Box key={item.id} pl={1}>
          <Icon style={{ color: item.color, fontSize: 50 }}>{item.icon}</Icon>
        </Box>
      ))}
    </Box>
  );
};
