import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import { Box, Typography, IconButton } from "@material-ui/core";
import RemoveIcon from "@material-ui/icons/Remove";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginTop: 30,
      marginBottom: 30,
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      backgroundColor: theme.palette.background.paper,
    },
  })
);
export interface TextCenterLargeProps {
  text: string;
}

export const TextCenterLarge: React.FC<TextCenterLargeProps> = ({ text }) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div style={{ width: "100%" }}>
        <Box
          m={2}
          mb={3}
          display="flex"
          flexDirection="column"
          textAlign="center"
        >
          <Box position="relative" m={1} mb={2}>
            <Typography component="div">
              <Box fontWeight={800} fontSize={25}>
                {text}
              </Box>
            </Typography>
          </Box>

          <Box position="absolute" alignSelf="center">
            <IconButton style={{ color: "#e10a0a", bottom: -60 }}>
              {<RemoveIcon style={{ fontSize: 65 }} />}
            </IconButton>
          </Box>
        </Box>
      </div>
    </div>
  );
};
