import { Home } from "./pages/Home";
import { Destinations } from "./pages/Destinations";
import { Deals } from "./pages/Deals";
import { Family } from "./pages/Family";
import { getUrl } from "./utils/getUrl";
import { Wishlist } from "./pages/Wishlist";
import { Email } from "./pages/Email";
import { Blog } from "./pages/Blog";
import { Car } from "./pages/Car";

interface PagesConfig {
  component: React.ReactElement;
  title: string;
  url: string;
}

const pagesConfig: Omit<PagesConfig, "url">[] = [
  { title: "Home", component: <Home /> },
  { title: "Destinations", component: <Destinations /> },
  { title: "Deals", component: <Deals /> },
  { title: "Family", component: <Family /> },
  { title: "Wishlist", component: <Wishlist /> },
  { title: "Email sign up", component: <Email /> },
  { title: "Blog", component: <Blog /> },

  { title: "Car Hire", component: <Car /> },
];

export const pages: PagesConfig[] = pagesConfig.map((page) => ({
  ...page,
  url: getUrl(page.title),
  // to={`/${
  //   page.title === "Home" ? "" : page.title.toLowerCase()
  // }`}
}));
