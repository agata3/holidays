import { IconBlog } from "./IconBlog";
import { MainBlogPicture } from "./MainBlogPicture";
import { MainTextBlog } from "./MainTextBlog";
import { List } from "../../components/List";
import { SimpleButtonGroup } from "./ButtonGroup";
import { BlogOffers } from "./BlogOffers";
import { offers } from "../../utils/offersData";
import { SimpleAccordion } from "../../components/SimpleAccordion";
import { infos } from "../../utils/infoData";
import { Footer } from "../../components/Footer";
import { blogModified } from "../../utils/blogModified";
import { SocialMedia } from "../../components/SocialMedia";

export const Blog: React.FC = () => {
  return (
    <div>
      <IconBlog />
      <MainBlogPicture />
      <MainTextBlog />
      <SimpleButtonGroup />
      <List Component={BlogOffers} list={offers} />
      <List Component={SimpleAccordion} list={blogModified(infos)} />
      <SocialMedia />
      <Footer />
    </div>
  );
};
