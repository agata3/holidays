import { makeStyles } from "@material-ui/core/styles";
import { Card, Box } from "@material-ui/core";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { Offer } from "../../utils/offersData";
const useStyles = makeStyles({
  root: {
    marginTop: 10,
    marginBottom: 10,
    paddingBottom: 10,
  },
});

interface PropsOffer {
  item: Offer;
}

export const BlogOffers: React.FC<PropsOffer> = ({ item }) => {
  const classes = useStyles();
  return (
    <Box mt={2} mb={2} display="flex">
      <Card className={classes.root}>
        <CardActionArea>
          <CardMedia
            component="img"
            alt="Contemplative Reptile"
            height="210"
            image={item.backgroundImage}
            title="Contemplative Reptile"
          />
          <CardContent>
            <Typography component="div">
              <Box
                mb={1}
                fontWeight="fontWeightLight"
                fontSize={14}
                color="#bdbdbd"
              >
                {item.data} | Virgin Holidays
              </Box>
            </Typography>

            <Typography
              gutterBottom
              variant="h5"
              component="h2"
              color="secondary"
              style={{ marginBottom: 12 }}
            >
              {item.title}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {item.text}
            </Typography>

            <Typography component="div" color="secondary">
              <Box pt={2} fontSize={18} fontWeight="fontWeightLight">
                Read More
              </Box>
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Box>
  );
};
