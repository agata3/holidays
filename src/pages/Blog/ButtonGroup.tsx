import React from "react";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { List } from "../../components/List";
import { nativeSelectsData } from "../../utils/nativeSelectsData";
import { NativeSelects } from "./NativeSelects";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root1: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      backgroundColor: "#f5f5f5",
      "& > *": {
        //   marginTop: theme.spacing(3),
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(1),
      },
    },
    root: {
      backgroundColor: "#f5f5f5",
      marginTop: theme.spacing(3),
      paddingBottom: theme.spacing(3),
    },
  })
);

export const SimpleButtonGroup = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.root1}>
        <ButtonGroup
          variant="text"
          color="primary"
          aria-label="text primary button group"
        >
          <Button>All posts</Button>
          <Button>Trending</Button>
          <Button>Our picks</Button>
        </ButtonGroup>
      </div>
      <List Component={NativeSelects} list={nativeSelectsData} />
    </div>
  );
};
