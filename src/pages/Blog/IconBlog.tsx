import { MainPicture } from "../../components/MainPicture";
import { useHomeData } from "../../utils/useHomeData";
import { Box } from "@material-ui/core";
export const IconBlog = () => {
  return (
    <Box mt={4}>
      <MainPicture
        height={useHomeData().mainBlogIcone.height}
        backgroundImage={useHomeData().mainBlogIcone.image}
        backgroundPositionX={useHomeData().mainBlogIcone.backgroundPositionX}
        backgroundPositionY={useHomeData().mainBlogIcone.backgroundPositionY}
      />
    </Box>
  );
};
