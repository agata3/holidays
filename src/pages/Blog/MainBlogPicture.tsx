import { MainPicture } from "../../components/MainPicture";
import { useHomeData } from "../../utils/useHomeData";
import Box from "@material-ui/core/Box";
export const MainBlogPicture = () => {
  return (
    <Box mt={1}>
      <MainPicture
        backgroundImage={useHomeData().mainBlogPicture.image}
        height={useHomeData().mainBlogPicture.height}
        backgroundPositionX={useHomeData().mainBlogPicture.backgroundPositionX}
      />
    </Box>
  );
};
