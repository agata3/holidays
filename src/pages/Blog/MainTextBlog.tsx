import { Typography } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";

export const MainTextBlog = () => {
  return (
    <Box
      style={{
        width: "100%",

        borderBottomColor: "#e0e0e0",
      }}
      borderRadius={5}
      borderBottom={2}
    >
      <Box display="flex" p={1} alignItems="flex-start">
        <Typography component="div" color="secondary">
          <Box p={1} flexGrow={1} fontSize={20}>
            How to get the most from your holiday
          </Box>
        </Typography>

        <Box p={1} alignSelf="flex-end">
          <ArrowForwardIosIcon color="secondary" />
        </Box>
      </Box>
      <Typography component="div">
        <Box ml={2} mb={2} fontWeight="fontWeightLight" fontSize={15}>
          20 July 2021 | Virgin Holidays
        </Box>
      </Typography>
    </Box>
  );
};
