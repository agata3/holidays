import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { FormControl, Box } from "@material-ui/core";
import NativeSelect from "@material-ui/core/NativeSelect";
import { Native } from "../../utils/nativeSelectsData";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      padding: theme.spacing(0, 2),
    },
    selectEmpty: {
      paddingTop: theme.spacing(2),
    },
  })
);

export interface PropsNative {
  item: Native;
}

export const NativeSelects: React.FC<PropsNative> = ({ item }) => {
  const classes = useStyles();
  const [state, setState] = React.useState<{
    age: string | number;
    name: string;
  }>({
    age: "",
    name: "hai",
  });

  const handleChange = (
    event: React.ChangeEvent<{ name?: string; value: unknown }>
  ) => {
    const name = event.target.name as keyof typeof state;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  return (
    <div style={{ backgroundColor: "#f5f5f5" }}>
      <Box display="flex" pb={1} pt={1}>
        <FormControl className={classes.formControl} fullWidth>
          <NativeSelect
            value={state.age}
            onChange={handleChange}
            name="age"
            className={classes.selectEmpty}
            inputProps={{ "aria-label": "age" }}
          >
            <option value="">{item.mainText}</option>
            <option value={10}>{item.option_1}</option>
            <option value={20}>{item.option_2}</option>
            <option value={30}>{item.option_3}</option>
          </NativeSelect>
        </FormControl>
      </Box>
    </div>
  );
};
