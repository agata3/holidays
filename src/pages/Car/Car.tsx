import { MainText } from "../../components/MainText";
import { MainTextPicture } from "../../components/MainTextPicture";
import { useHomeData } from "../../utils/useHomeData";
import { CheckText } from "./CheckText";
import { CheckTextSecond } from "./CheckTextSecond";
import { List } from "../../components/List";
import { CardDownButton } from "../../components/CardDownButton";
import { TextCenterLarge } from "../../components/TextCenterLarge";
import { cars } from "../../utils/carData";
import { SimpleAccordion } from "../../components/SimpleAccordion";
import { carAccordionData } from "../../utils/carData";
import { SocialMedia } from "../../components/SocialMedia";
import { Footer } from "../../components/Footer";

export const Car = () => {
  let homeData = useHomeData();
  return (
    <>
      <MainTextPicture
        backgroundImage={homeData.mainCarTextPicture.backgroundImage}
        height={homeData.mainCarTextPicture.height}
        text={homeData.mainCarTextPicture.text}
        backgroundPositionX={homeData.mainCarTextPicture.backgroundPositionX}
      />
      <MainText
        mainText={homeData.mainCarText.mainText}
        secondaryText={homeData.mainCarText.secondaryText}
      />

      <CheckText />
      <CheckTextSecond />

      <TextCenterLarge text={homeData.textCenterLargeCar.text} />
      <List Component={CardDownButton} list={cars} />
      <List Component={SimpleAccordion} list={carAccordionData} />
      <SocialMedia />
      <Footer />
    </>
  );
};
