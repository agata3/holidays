import CheckIcon from "@material-ui/icons/Check";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import MaximizeIcon from "@material-ui/icons/Maximize";

export const CheckText = () => {
  return (
    <div style={{ width: "100%", marginBottom: 90 }}>
      <Typography component="div">
        <Box m={1} fontSize={20} fontWeight="fontWeightBold">
          Our exclusive Alamo car hire packages in the USA include:
        </Box>
        <Box mb={1} m={1}>
          <MaximizeIcon color="secondary" fontSize="large" />
        </Box>
        <Box display="flex">
          <Box>
            <CheckIcon color="primary" />
          </Box>

          <Box
            fontSize={16}
            fontWeight="fontWeightBold"
            marginLeft={1}
            marginRight={2}
          >
            Dolor sit amet consectetur adipisicing elit.
          </Box>
        </Box>
        <Box
          fontSize={13}
          fontWeight="fontWeightLight"
          marginLeft={5}
          marginRight={2}
          marginY={1}
        >
          Members receive bonus Virgin points, simply present your membership
          card upon arrival
        </Box>
        <Box display="flex" marginTop={3}>
          <Box>
            <CheckIcon color="primary" />
          </Box>

          <Box fontSize={16} fontWeight="fontWeightBold" marginLeft={1}>
            Our exclusive Alamo car hire packages
          </Box>
        </Box>
        <Box
          fontSize={13}
          fontWeight="fontWeightLight"
          marginLeft={5}
          marginRight={2}
          marginY={1}
        >
          Members receive bonus Virgin points, simply present your membership
          card upon arrival
        </Box>
        <Box display="flex" marginTop={3}>
          <Box>
            <CheckIcon color="primary" />
          </Box>

          <Box fontSize={16} fontWeight="fontWeightBold" marginLeft={1}>
            Lorem ipsum dolor sit amet.
          </Box>
        </Box>
        <Box
          fontSize={13}
          fontWeight="fontWeightLight"
          marginLeft={5}
          marginRight={2}
        >
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic adipisci,
          vero quibusdam maiores optio omnis rem labore assumenda nisi enim
          cupiditate veniam.
        </Box>
        <Box display="flex" marginTop={3}>
          <Box>
            <CheckIcon color="primary" />
          </Box>

          <Box fontSize={16} fontWeight="fontWeightBold" marginLeft={1}>
            Consectetur adipisicing elit. Inventore, at.
          </Box>
        </Box>
        <Box
          fontSize={13}
          fontWeight="fontWeightLight"
          marginLeft={5}
          marginRight={2}
          marginY={1}
        >
          Dolor sit amet consectetur adipisicing elit. Hic adipisci,
        </Box>
        <Box display="flex" marginTop={4}>
          <Box>
            <CheckIcon color="primary" />
          </Box>

          <Box fontSize={16} fontWeight="fontWeightBold" marginLeft={1}>
            Consectetur adipisicing
          </Box>
        </Box>
        <Box
          fontSize={13}
          fontWeight="fontWeightLight"
          marginLeft={5}
          marginRight={2}
        >
          Dolor sit amet consectetur adipisicing elit.
        </Box>
      </Typography>
    </div>
  );
};
