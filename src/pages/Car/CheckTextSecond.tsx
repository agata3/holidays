import CheckIcon from "@material-ui/icons/Check";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import MaximizeIcon from "@material-ui/icons/Maximize";

export const CheckTextSecond = () => {
  return (
    <div style={{ width: "100%" }}>
      <Typography component="div">
        <Box m={1} fontSize={20} mb={-1} fontWeight="fontWeightBold">
          Plus in Florida:
        </Box>
        <Box m={1}>
          <MaximizeIcon color="secondary" fontSize="large" />
        </Box>
        <Box display="flex">
          <Box>
            <CheckIcon color="primary" />
          </Box>

          <Box
            fontSize={16}
            fontWeight="fontWeightBold"
            marginLeft={1}
            marginRight={2}
          >
            Free delivery
          </Box>
        </Box>
        <Box
          fontSize={13}
          fontWeight="fontWeightLight"
          marginLeft={5}
          marginRight={2}
          marginY={1}
          mb={3}
        >
          Members receive bonus Virgin points, simply present your membership
        </Box>

        <Box m={1} fontSize={20} fontWeight="fontWeightBold" mb={-1}>
          Plus in California and Las Vegas:
        </Box>
        <Box m={1}>
          <MaximizeIcon color="secondary" fontSize="large" />
        </Box>
        <Box display="flex">
          <Box>
            <CheckIcon color="primary" />
          </Box>

          <Box
            fontSize={16}
            fontWeight="fontWeightBold"
            marginLeft={1}
            marginRight={2}
          >
            No one way drop off fees
          </Box>
        </Box>
        <Box
          fontSize={13}
          fontWeight="fontWeightLight"
          marginLeft={5}
          marginRight={2}
          marginY={1}
        >
          Members receive bonus Virgin points, simply present your membership
        </Box>
      </Typography>
    </div>
  );
};
