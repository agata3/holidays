import { MainTextPicture } from "../../components/MainTextPicture";
import { DealsOffer } from "./DealsOffers";
import { Footer } from "../../components/Footer";
import { SocialMedia } from "../../components/SocialMedia";
import { useHomeData } from "../../utils/useHomeData";
import { MainText } from "../../components/MainText";
import Payment from "../../components/Payment";

export const Deals: React.FC = () => {
  const homeData = useHomeData();
  return (
    <>
      <MainTextPicture
        height={homeData.mainDealsTextPicture.height}
        text={homeData.mainDealsTextPicture.text}
        backgroundImage={homeData.mainDealsTextPicture.backgroundImage}
      />
      <MainText
        mainText={homeData.mainDealText.mainText}
        secondaryText={homeData.mainDealText.secondaryText}
      />
      <DealsOffer />
      <SocialMedia />
      <Footer />
      <Payment />
    </>
  );
};
