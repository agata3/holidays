import React from "react";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { DealsOffersItem } from "./DealsOffersItem";
import { useDeals } from "../../utils/useDeals";
import { List } from "../../components/List";

const useStyles = makeStyles({
  root: {
    position: "relative",
    top: -20,
    borderTop: 1,
    paddingTop: 4,
    marginBottom: 2,
  },
});

export const DealsOffer: React.FC = () => {
  const classes = useStyles();
  const deals = useDeals();
  return (
    <Box className={classes.root}>
      <List Component={DealsOffersItem} list={deals} />
    </Box>
  );
};
