import React from "react";
import { Footer } from "../../components/Footer";
import { SocialMedia } from "../../components/SocialMedia";
import { TextDestination } from "./TextDestination";
import { SingleLineGridList } from "../../components/SingleLineGridList";
import { MainTextPicture } from "../../components/MainTextPicture";
import { useHomeData } from "../../utils/useHomeData";
import { List } from "../../components/List";
import { CardDownButton } from "../../components/CardDownButton";
import { useDeals } from "../../utils/useDeals";
import { deals } from "../../utils/dealsData";
import Payment from "../../components/Payment";
export const Destinations: React.FC = () => {
  const homeData = useHomeData();
  const dealsChangePicture = useDeals();
  return (
    <>
      <MainTextPicture
        height={homeData.mainDestinationTextPicture.height}
        backgroundImage={homeData.mainDestinationTextPicture.backgroundImage}
        text={homeData.mainDestinationTextPicture.text}
        backgroundPositionY={
          homeData.mainDestinationTextPicture.backgroundPositionY
        }
      />
      <TextDestination />

      <List Component={CardDownButton} list={deals} />
      <SingleLineGridList list={dealsChangePicture} />
      <SocialMedia />
      <Footer />
      <Payment />
    </>
  );
};
