import { Box, Typography } from "@material-ui/core";

export const TextDestination = () => {
  return (
    <Box display="flex" p={2}>
      <Typography component="div">
        <Box
          fontSize={28}
          m={2}
          textAlign="center"
          style={{ color: "black" }}
          fontWeight={800}
        >
          Where to go on holiday?
        </Box>
        <Box
          textAlign="justify"
          m={2}
          fontSize={14}
          style={{ textIndent: "1.5em" }}
        >
          We’re the UK’s largest and most successful transatlantic tour operator
          and have
          <strong style={{ color: "#067b93" }}> 30 years’ experience</strong> as
          a long-haul package holiday company.
        </Box>
        <Box
          textAlign="left"
          m={2}
          fontSize={14}
          style={{ textIndent: "1.5em" }}
        >
          Protection does not apply to all{" "}
          <strong style={{ color: "#067b93" }}>
            holiday and travel services
          </strong>{" "}
          listed on this website.
        </Box>
      </Typography>
    </Box>
  );
};
