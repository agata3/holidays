import React from "react";
import { Footer } from "../../components/Footer";
import { MainText } from "../../components/MainText";
import { MainTextPicture } from "../../components/MainTextPicture";
import { SocialMedia } from "../../components/SocialMedia";
import { TextCenterLarge } from "../../components/TextCenterLarge";
import { useInfos } from "../../utils/useInfos";
import { useHomeData } from "../../utils/useHomeData";
import { SimpleAccordion } from "../../components/SimpleAccordion";
import { List } from "../../components/List";
import { FamilyCard } from "./FamilyCard";
import { familyData } from "../../utils/familyData";
import { SingleLineGridList } from "../../components/SingleLineGridList";
import { useDeals } from "../../utils/useDeals";
import Payment from "../../components/Payment";

export const Family: React.FC = () => {
  const homeData = useHomeData();
  const infos = useInfos();
  return (
    <>
      <MainTextPicture
        height={homeData.mainFamilyTextPicture?.height || 400}
        backgroundImage={homeData?.mainFamilyTextPicture?.backgroundImage}
        text={homeData.mainFamilyTextPicture.text}
        backgroundPositionX={homeData.mainFamilyTextPicture.backgroundPositionX}
      />
      <MainText
        mainText={homeData.mainFamilyText.mainText}
        secondaryText={homeData.mainFamilyText.secondaryText}
      />
      {/* {infos.map((info) => (
        <SimpleAccordion info={info} />
      ))} */}

      <List Component={SimpleAccordion} list={infos} />
      <TextCenterLarge text={homeData.textCenterLarge.text} />
      <List Component={FamilyCard} list={familyData} />
      <SingleLineGridList list={useDeals()} />
      <SocialMedia />
      <Footer />
      <Payment />
    </>
  );
};
