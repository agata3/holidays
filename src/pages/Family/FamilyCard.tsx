import { makeStyles } from "@material-ui/core/styles";
import { Card, Box } from "@material-ui/core";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { FamilyInfo } from "../../utils/familyData";

const useStyles = makeStyles({
  root: {},
});

interface Family {
  item: FamilyInfo;
}

export const FamilyCard: React.FC<Family> = ({ item }) => {
  const classes = useStyles();
  return (
    <Box mt={2} mb={2} display="flex" key={item.key}>
      <Card className={classes.root}>
        <CardActionArea>
          <CardMedia
            component="img"
            alt="Contemplative Reptile"
            height="210"
            image={item.backgroundImage}
            title="Contemplative Reptile"
          />
          <CardContent style={{ backgroundColor: "#eeeeee" }}>
            <Typography variant="body2" color="textPrimary" component="div">
              <Box textAlign="center" fontWeight="bold" fontSize={16}>
                {item.text}
              </Box>
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Box>
  );
};
