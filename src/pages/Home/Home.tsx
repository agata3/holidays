import React from "react";
import { IconsText } from "./IconsText";
import { MainPicture } from "../../components/MainPicture";
import { SocialMedia } from "../../components/SocialMedia";
import { Footer } from "../../components/Footer";
import { SingleLineGridList } from "../../components/SingleLineGridList";
import { dealsModified } from "../../utils/dealsModified";
import { useDeals } from "../../utils/useDeals";
import { useHomeData } from "../../utils/useHomeData";
import Payment from "../../components/Payment";

export const Home: React.FC = () => {
  const homeData = useHomeData();
  const deals = useDeals();

  return (
    <>
      <MainPicture
        height={homeData.mainHomePicture?.height || 200}
        backgroundImage={homeData?.secondHomePicture?.image || ""}
      ></MainPicture>
      <IconsText></IconsText>
      <MainPicture
        height={homeData.secondHomePicture.height}
        backgroundImage={homeData.mainHomePicture.image}
      ></MainPicture>
      <SingleLineGridList list={dealsModified(deals)} />
      <SocialMedia />
      <Footer />
      <Payment />
    </>
  );
};
