import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Box, Card, Icon } from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import AccessibilityNewIcon from "@material-ui/icons/AccessibilityNew";
import MoodIcon from "@material-ui/icons/Mood";
import CreditCardIcon from "@material-ui/icons/CreditCard";
import BusinessCenterIcon from "@material-ui/icons/BusinessCenter";

const lists = [
  {
    id: 1,
    mainText: "You are in safe hands",
    secondaryText: "We can guarante peace of mind .",
    icon: <AccessibilityNewIcon />,
  },
  {
    id: 2,
    mainText: "Protection",
    secondaryText: "Our package holidays.",
    icon: <MoodIcon />,
  },
  {
    id: 3,
    mainText: "Direct Debit",
    secondaryText: "Spread the cost of your holiday.",
    icon: <CreditCardIcon />,
  },
  {
    id: 4,
    mainText: "Ready to rabook?",
    secondaryText: "Log in to change your holidays plans.",
    icon: <BusinessCenterIcon />,
  },
];

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 10,
  },
  pos: {
    marginBottom: 10,
  },
});

export const IconsText = () => {
  const classes = useStyles();
  //   const bull = <span className={classes.bullet}>•</span>;

  return (
    <div style={{ width: "100%" }}>
      <Box mt={2} mb={2}>
        {lists.map((list) => (
          <Box key={list.id}>
            <Card className={classes.root} variant="outlined">
              <CardContent>
                <div style={{ width: "100%" }}>
                  <Box
                    p={1}
                    display="flex"
                    flexDirection="column"
                    // justifyContent="center"
                  >
                    <Box alignSelf="center">
                      <Icon fontSize="large">{list.icon}</Icon>
                    </Box>
                    <Box alignSelf="center">
                      <Typography variant="h6" component="h3">
                        {list.mainText}
                      </Typography>
                    </Box>
                    <Box alignSelf="center">
                      <Typography variant="body2" component="p">
                        {list.secondaryText}
                      </Typography>
                    </Box>
                  </Box>
                </div>
              </CardContent>
            </Card>
          </Box>
        ))}
      </Box>
    </div>
  );
};
