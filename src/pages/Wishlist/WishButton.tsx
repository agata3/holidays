import { Button, Box } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
export const WishButton = () => {
  return (
    <div>
      <Box display="flex" justifyContent="center" alignItems="center" mb={3}>
        <Button
          size="medium"
          variant="contained"
          style={{
            backgroundColor: "rgb(225, 10, 10)",
            color: "white",
            textAlign: "center",
            fontWeight: "bolder",
            padding: 10,
          }}
          endIcon={<AddIcon style={{ color: "white" }}></AddIcon>}
        >
          Create a new list
        </Button>
      </Box>
    </div>
  );
};
