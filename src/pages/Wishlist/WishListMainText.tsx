import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

export const WishListMainText = () => {
  return (
    <div style={{ width: "100%", backgroundColor: "rgb(225, 10, 10)" }}>
      <Box display="flex" textAlign="center" p={2}>
        <Typography component="div">
          <Box m={1} fontWeight={800} fontSize={43} color="white">
            Wishlist
          </Box>
          <Box fontSize={16} color="white">
            A space to save your faves.If it gives your a warm feeling- save it
            here.
          </Box>
        </Typography>
      </Box>
    </div>
  );
};
