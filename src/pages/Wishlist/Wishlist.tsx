import { MainTextPicture } from "../../components/MainTextPicture";
import { useHomeData } from "../../utils/useHomeData";
import { WishListMainText } from "./WishListMainText";
import { MainText } from "../../components/MainText";
import { WishButton } from "./WishButton";
import { SimpleAccordion } from "../../components/SimpleAccordion";
import { useInfos } from "../../utils/useInfos";
import { List } from "../../components/List";
import { SocialMedia } from "../../components/SocialMedia";
import { Footer } from "../../components/Footer";
import Payment from "../../components/Payment";
export const Wishlist: React.FC = () => {
  const infos = useInfos();
  const homeData = useHomeData();
  return (
    <div>
      <WishListMainText />
      <MainTextPicture
        height={homeData.mainWishlistPicture.height}
        backgroundImage={homeData.mainWishlistPicture.backgroundImage}
        backgroundPositionY={homeData.mainWishlistPicture.backgroundPositionY}
      />

      <MainText
        mainText={homeData.mainWishText.mainText}
        secondaryText={homeData.mainWishText.secondaryText}
      />
      <WishButton />
      <List Component={SimpleAccordion} list={infos} />

      <SocialMedia />

      <Footer />
      <Payment />
    </div>
  );
};
