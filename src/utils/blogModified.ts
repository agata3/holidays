import { Info } from "./infoData";

export const blogModified = (infos: Info[]) => {
  return infos.map((info) => ({
    ...info,
    mainText: info.mainTextBlog,
    color: "#e10a0a",
  }));
};
