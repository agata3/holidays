import car from "../assets/car.jpg";

interface Car {
  key: number;
  title: string;
  read: string;
  text: string;
  backgroundImage: string;
}

export const cars: Car[] = [
  {
    key: 1,
    title: " Canada Car Hire",
    read: "Read more",
    text:
      "These  blissful weather, these close-knit pieces of paradise are a dream to explore.",
    backgroundImage:
      "https://www.virginholidays.co.uk/.imaging/Desktop/dam/cdc0a2dc-f3d5-4bbc-8173-769fcdc14720.jpg",
  },
  {
    key: 2,
    title: "Adopted car and van hire",
    text:
      "hundreds of islands where scenes  and luxuriously warm seas become a reality. Blessed by spectacular underwater life and blissful weather,  of paradise are a dream to explore.",

    read: "Read more",
    backgroundImage:
      "https://www.virginholidays.co.uk/.imaging/Desktop/dam/0b2671a7-425e-41e1-9f8c-5239e0790c51.jpg",
  },
  {
    key: 3,
    title: "Car rental for the weekend",
    text:
      "These 26 coral atolls&nbsp;consist&nbsp;of hundreds of islands where scenes of crystal.",

    read: "Read more",
    backgroundImage:
      "https://www.virginholidays.co.uk/.imaging/SquareCarousel/dam/0beabbf0-e96a-4f3f-9763-84932f64a9ad.jpg",
  },

  {
    key: 4,
    title: "Our cars",
    text:
      " Hundreds of islands where  white sands and luxuriously warm seas become a reality. Blessed by spectacular underwater life and blissful weather, these close-knit pieces of paradise are a dream to explore.",

    read: "Read more",
    backgroundImage: `${car}`,
  },
];

interface CarAccordionData {
  key: number;
  mainText: string;
  secondaryText: string;

  color: string;
}

export const carAccordionData: CarAccordionData[] = [
  {
    key: 1,
    color: "rgb(6, 123, 147)",
    mainText: "Car Hire Terms and Conditions",

    secondaryText:
      "At Enterprise Rent-A-Car, we pride ourselves on great customer service. We always go the extra mile to make sure renting vehicles is as easy as possible, whether for leisure or business, all over the world. consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis egetAt Enterprise Rent-A-Car, we pride ourselves on great customer service. We always go the extra mile to make sure renting vehicles is as easy as possible, whether for leisure or business, all over the world. consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.",
  },
];
