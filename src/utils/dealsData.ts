export interface Deal {
  backgroundImage: string;
  key: number;
  price: string;
  text: string;
  title: string;
}

export const deals: Deal[] = [
  {
    key: 1,
    title: "  Maledives holidays",
    price: " $ 1449pp",
    text:
      "These  blissful weather, these close-knit pieces of paradise are a dream to explore.",
    backgroundImage:
      "https://gateway.virginholidays.co.uk/image-processor?imageSrc=https%3A%2F%2Fwww.virginholidays.co.uk%2Fdam%2Fvirginholidays%2Fhomepage-promo-pods%2Fpersonalisation%2Fmaldives.jpg&amp;format=jpg&amp;dimensions=591x298",
  },
  {
    key: 2,
    title: "Barbados holidays",
    text:
      "hundreds of islands where scenes  and luxuriously warm seas become a reality. Blessed by spectacular underwater life and blissful weather,  of paradise are a dream to explore.",

    price: " $ 899pp",
    backgroundImage:
      "https://gateway.virginholidays.co.uk/image-processor?imageSrc=https%3A%2F%2Fwww.virginholidays.co.uk%2Fdam%2Fvirginholidays%2Fhomepage-promo-pods%2Fhomepage-promo-pods-sale-lockups%2Fweb-peak-collateral_core_promo-pod_barbados_no-text.jpg&amp;format=jpg&amp;dimensions=591x298",
  },
  {
    key: 3,
    title: "  Antigua",
    text:
      "These 26 coral atolls&nbsp;consist&nbsp;of hundreds of islands where scenes of crystal.",

    price: " $ 1199pp",
    backgroundImage:
      "https://www.virginholidays.co.uk/.imaging/Desktop/dam/160d7679-a2aa-4d1b-bada-541254cfde73.jpg",
  },
  {
    key: 4,
    title: " Orlando weekend",
    text:
      "These 26 of crystal white  seas become a reality. Blessed by spectacular underwater life and blissful weather, these close-knit pieces of paradise are a dream to explore.",

    price: " $ 449pp",
    backgroundImage:
      "//d3hk78fplavsbl.cloudfront.net/assets/common-prod/hotel/205/27679/27679-1-results_carousel.jpg?version=40",
  },
  {
    key: 5,
    title: "New York city break",
    text:
      " Hundreds of islands where  white sands and luxuriously warm seas become a reality. Blessed by spectacular underwater life and blissful weather, these close-knit pieces of paradise are a dream to explore.",

    price: " $ 849pp",
    backgroundImage:
      "https://gateway.virginholidays.co.uk/image-processor?imageSrc=https%3A%2F%2Fwww.virginholidays.co.uk%2Fdam%2Fvirginholidays%2Fhomepage-promo-pods%2FNYC.jpg&amp;format=jpg&amp;dimensions=591x298",
  },
];
