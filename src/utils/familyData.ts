export interface FamilyInfo {
  backgroundImage: string;
  key: number;
  text: string;
}

export const familyData: FamilyInfo[] = [
  {
    key: 1,
    text: "Walt Disnay World Resort",
    backgroundImage:
      "https://www.virginholidays.co.uk/.imaging/Desktop/dam/05935231-4e89-41a6-93d4-1969260dcded.jpg",
  },
  {
    key: 2,
    text: "Florida villas",
    backgroundImage:
      "https://www.virginholidays.co.uk/.imaging/Desktop/dam/b8235137-25cc-4a08-89a8-1242381b63b9.jpg",
  },
  {
    key: 3,
    text: "Antigua all inclusive",
    backgroundImage:
      "https://www.virginholidays.co.uk/.imaging/Desktop/dam/4faaa3ee-a1a4-4d0b-8c20-c07b592c7173.jpg",
  },
  {
    key: 4,
    text: "Universal Orlando Resort",
    backgroundImage:
      "https://www.virginholidays.co.uk/.imaging/Phone/dam/aebb9533-2359-428f-980b-11adbba00209.jpg",
  },
];
