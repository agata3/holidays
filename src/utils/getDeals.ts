import { Deal } from "./dealsData";

export interface DealWithoutTitle extends Omit<Deal, "title"> {}

export const getDeals = (deals: Deal[]) => {
  return deals.map((deal) => ({
    ...deal,
    backgroundImage:
      deal.backgroundImage ===
      "https://gateway.virginholidays.co.uk/image-processor?imageSrc=https%3A%2F%2Fwww.virginholidays.co.uk%2Fdam%2Fvirginholidays%2Fhomepage-promo-pods%2Fhomepage-promo-pods-sale-lockups%2Fweb-peak-collateral_core_promo-pod_barbados_no-text.jpg&amp;format=jpg&amp;dimensions=591x298"
        ? "https://www.virginholidays.co.uk/.imaging/Phone/dam/fa2f885a-127d-4b58-ac6d-7b87e70e316e.jpg"
        : deal.backgroundImage,
    title: deal.title === "Barbados holidays" ? "Orlando Resort" : deal.title,
  }));
};
// import { getDeals } from "./getDeals";
// import { deals } from "./dealsData";

// export const useDeals = () => getDeals(deals);
