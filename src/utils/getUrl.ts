export const getUrl = (title: string) => {
  return `/${title === "Home" ? "" : title.toLowerCase()}`;
};
