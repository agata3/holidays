export interface Info {
  key: number;
  mainText?: string;
  secondaryText?: string;
  mainTextBlog?: string;
  color: string;
}
export const infos: Info[] = [
  {
    key: 1,
    color: "rgb(6, 123, 147)",
    mainText: "Disney dreams",
    mainTextBlog: " Enhance Your Holiday",
    secondaryText:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.",
  },
  {
    key: 2,
    color: "rgb(6, 123, 147)",
    mainText: "Hotel for every family",
    mainTextBlog: "Policies",
    secondaryText:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.",
  },
  {
    key: 3,
    color: "rgb(6, 123, 147)",
    mainText: "Activities for everyone",
    mainTextBlog: "About Us",
    secondaryText:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.",
  },
  {
    key: 4,
    color: "rgb(6, 123, 147)",
    mainText: "Luxury family holidays",
    mainTextBlog: "Travel information",
    secondaryText:
      " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.",
  },
];
