export interface Native {
  key: number;
  mainText: string;
  option_1: string;
  option_2: string;
  option_3: string;
}

export const nativeSelectsData: Native[] = [
  {
    key: 1,
    mainText: "Region",
    option_1: "Ten",
    option_2: "Twenty",
    option_3: "Thirty",
  },
  {
    key: 1,
    mainText: "Destination",
    option_1: "Mexico",
    option_2: "Barbados",
    option_3: "Dublin",
  },
  {
    key: 1,
    mainText: "Holiday type",
    option_1: "Family",
    option_2: "Luxury",
    option_3: "Beach",
  },
  {
    key: 1,
    mainText: "Interest",
    option_1: "Relax",
    option_2: "Events",
    option_3: "Party",
  },
];
