export interface Offer {
  backgroundImage: string;
  key: number;
  data: string;
  text: string;
  title: string;
}

export const offers: Offer[] = [
  {
    key: 1,
    title: " 5 days trips to take when in Maledives ",
    data: " 20 July 2022 ",
    text:
      "These  blissful weather, these close-knit pieces of paradise are a dream to explore.",
    backgroundImage:
      "https://www.virginholidays.co.uk/dam/jcr:3f9444b4-0b02-440a-a9f1-65e0e855c120",
  },
  {
    key: 2,
    title: "An expert's guide on things to do in  Barbados ",
    data: " 22 June 2022 ",
    text:
      "hundreds of islands where scenes  and luxuriously warm seas become a reality. Blessed by spectacular underwater life and blissful weather,  of paradise are a dream to explore.",

    backgroundImage:
      "https://www.virginholidays.co.uk/dam/jcr:5d3be561-fdf8-491c-b739-bc061ba39b4a",
  },
  {
    key: 3,
    title: "To the Copa... Copacabana",
    data: " 01 Oct 2022 ",
    text:
      "These 26 coral atolls&nbsp;consist&nbsp;of hundreds of islands where scenes of crystal.",

    backgroundImage:
      "https://www.virginholidays.co.uk/.imaging/Desktop/dam/160d7679-a2aa-4d1b-bada-541254cfde73.jpg",
  },
  {
    key: 4,
    title: " Consider Cambodia",
    data: " 14 June 2022 ",
    text:
      "These 26 of crystal white  seas become a reality. Blessed by spectacular underwater life and blissful weather, these close-knit pieces of paradise are a dream to explore.",

    backgroundImage:
      "https://www.virginholidays.co.uk/dam/jcr:3354d7a7-6cb5-4259-8a77-e1fb04d661ce",
  },
  {
    key: 5,
    title: "What you need to know about Thailand",
    data: " 02 Sep 2022 ",
    text:
      " Hundreds of islands where  white sands and luxuriously warm seas become a reality. Blessed by spectacular underwater life and blissful weather, these close-knit pieces of paradise are a dream to explore.",

    backgroundImage:
      "https://www.virginholidays.co.uk/dam/jcr:f4821264-9696-4797-ac73-89294f2015b5",
  },
];
