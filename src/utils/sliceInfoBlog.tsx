import { Info } from "./infoData";

export const sliceInfoBlog = (infos: Info[]) => {
  return infos.slice(0, 4);
};
