import { getDeals } from "./getDeals";
import { deals } from "./dealsData";

export const useDeals = () => getDeals(deals);

//*   getDeals
// export const getDeals = (deals: Deal[]) => {
//     return deals.map((deal) => ({
//       ...deal,
//       title: deal.title === "New York city break" ? "Hello" : deal.title,
//     }));
//   };
