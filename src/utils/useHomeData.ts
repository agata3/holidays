// import { useEffect } from "react";
// import axios from "axios";
// import { useState } from "react";
import family_text_main from "../assets/family_text_main.jpg";
import wishlist from "../assets/wishlist.jpg";
import road from "../assets/road.jpg";

export const useHomeData = () => {
  const homeData = {
    mainHomePicture: {
      image:
        "https://gateway.virginholidays.co.uk/image-processor?imageSrc=https%3A%2F%2Fwww.virginholidays.co.uk%2Fdam%2Fvirginholidays%2Fspecial-offers%2Fheader%2FVAH_Landing-Page_1610x600.jpg&amp;format=jpg",
      height: 200,
    },
    secondHomePicture: {
      image:
        "https://gateway.virginholidays.co.uk/image-processor?imageSrc=https%3A%2F%2Fwww.virginholidays.co.uk%2Fdam%2Fvirginholidays%2Fhomepage-carousel%2F2316x696-VH-Desktop-Homepage_Peace-of-mind-%28004%29.jpg&format=jpg",
      height: 220,
    },
    mainFamilyTextPicture: {
      backgroundImage: `${family_text_main}`,
      height: 400,
      text: "Family Holidays",
      backgroundPositionX: -150,
    },
    mainDestinationTextPicture: {
      backgroundImage:
        "https://www.virginholidays.co.uk/.imaging/Phone/dam/2a53eb36-0541-4f0e-85ae-e3c42bd30659.jpg",
      height: 300,
      text: "Our holiday destination",
      backgroundPositionY: -100,
    },
    mainDealsTextPicture: {
      backgroundImage:
        "https://www.virginholidays.co.uk/.imaging/Phone/dam/2eb6076b-e552-4936-ab4c-62fcbea95713.jpg",
      height: 460,
      text: "Holidays in 2022",
    },
    mainDealText: {
      mainText: " Book your holiday early and relax",
      secondaryText:
        " Sapiente dolores accuvelit molestiae architecto recusandae neque namaliquam .",
    },
    mainFamilyText: {
      mainText: " Family holidays fun for all ages",
      secondaryText:
        " We know how precious time together is, and that’s why we are experts in making sure there really is something for everyone on your dream family holiday.",
    },
    textCenterLarge: {
      text: "Our family friendly holidays",
    },
    mainWishlistPicture: {
      backgroundImage: `${wishlist}`,
      height: 180,

      backgroundPositionY: -40,
      // backgroundPositionX?:
    },
    mainWishText: {
      mainText: " Get started",
      secondaryText:
        " Sapiente dolores accuvelit molestiae architecto recusandae ,dolores accuvelit molestiae architecto recusandae .",
    },
    mainBlogIcone: {
      image:
        "https://www.virginholidays.co.uk/.imaging/Phone/dam/cc152a1a-67a5-4387-8be0-de63e6dc9358.png",
      height: 100,
      backgroundPositionX: -10,
      backgroundPositionY: -7,
    },
    mainBlogPicture: {
      image:
        "https://www.virginholidays.co.uk/.imaging/Phone/dam/ea32c4b0-03b0-4d1d-b812-a7600cf8e1ee.jpg",
      height: 200,
      backgroundPositionX: -35,
    },
    mainCarTextPicture: {
      backgroundImage: `${road}`,
      height: 450,
      text: "No Fuss Car hire",
      backgroundPositionX: -200,
    },
    mainCarText: {
      mainText: " Car hire",
      secondaryText:
        "Everyone loves their independence. Wherever you’re holidaying, car rental is a great way to get out and about and explore the harder to reach places or neighbouring towns without having to worry about navigating local public transport.",
    },
    textCenterLargeCar: {
      text: "Try our car hire locations",
    },
  };

  return homeData;
};

// const [homeData, setHomeData] = useState<{
//   mainPicture?: { image: string; height: number };
// }>({});

// useEffect(() => {
//   const effect = async () => {
//     const { data } = await axios.get("http://localhost:3001/mainPicture");
//     setHomeData({ mainPicture: data });
//   };
//   effect();
// }, []);
